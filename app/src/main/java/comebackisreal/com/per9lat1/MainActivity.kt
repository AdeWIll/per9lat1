package comebackisreal.com.per9lat1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnbuat.setOnClickListener {
            startActivityForResult(Intent(this, buat::class.java),0)
        }

        btnbaca.setOnClickListener {
            startActivity(Intent(this, baca::class.java))
        }
    }
}
