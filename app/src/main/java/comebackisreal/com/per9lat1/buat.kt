package comebackisreal.com.per9lat1

import android.app.backup.FileBackupHelper
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.*
import kotlinx.android.synthetic.main.activity_buat.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.Month
import java.time.Year
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.log

class buat : AppCompatActivity() {
    private var cal = Calendar.getInstance()

    private fun format() : String{
        val myFormat = "dd MMM yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat)
        return  sdf.format(cal.getTime())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buat)

        var myFileHelper = FileHelper()

        val linearLayout = findViewById(R.id.linearLayout) as LinearLayout
        val datePicker = DatePicker(this)

        cal.timeInMillis = System.currentTimeMillis()
        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),
            DatePicker.OnDateChangedListener{
                datePicker, year, month, day->cal.set(year, month, day)
            }
        )
        linearLayout.addView(datePicker)

        btnsimpan.setOnClickListener {
            var tanggal : String = format()
            var pesan = EditTextIsi.text.toString().replace(';',':')

            var oldData = ""
            try{
                oldData = myFileHelper.readFile(this)
            }catch (e:Exception){
                Log.w("TAG",e)
                oldData = ""
            }finally {
                myFileHelper.writeFile(tanggal+";"+pesan, this, oldData)
                Toast.makeText(this, "Data berhasil disimpan pada "+tanggal+" dengan pesan "+pesan, Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}
