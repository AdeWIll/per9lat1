package comebackisreal.com.per9lat1

import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_baca.*
import kotlinx.android.synthetic.main.activity_main.*
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*

class baca : AppCompatActivity() {
    private var cal = Calendar.getInstance()

    private fun format() : String{
        val myFormat = "dd MMM yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat)
        return  sdf.format(cal.getTime())
    }

    private fun myDatePicker() :DatePickerDialog.OnDateSetListener{
        val dateSetListener = object : DatePickerDialog.OnDateSetListener{
            override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
                cal.set(Calendar.YEAR,year)
                cal.set(Calendar.MONTH,month)
                cal.set(Calendar.DAY_OF_MONTH,day)
                TxtVIewhari.text = format()
            }

        }
        return dateSetListener;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baca)

        var myFileHelper = FileHelper()

        pilih.setOnClickListener{
            DatePickerDialog(this,
                myDatePicker(),
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        bacamemo.setOnClickListener {
            if(TxtVIewhari.text.toString().equals("dd MM yyyy")){
                Toast.makeText(this, "Harap pilih tanggal", Toast.LENGTH_SHORT).show()
            }
            else{
                //baca filenya
                var file = myFileHelper.readFile(this)
                //pecah filenya jadi array berdasarkan \n (enter) atau dgn kata lain per baris
                var arrayLine = file.lines()

                //buat temp pesan untuk tampilkan ke UI
                var pesan = ""
                //ini kalo g buat jg gpp
                var ketemu = false

                //perulangan kedalam arraynya
                for(i in arrayLine){
                    //isi tiap index arraynya contohnya "17 May 2019;asdasda"
                    //pisah antara tanggal sama isi pesannya pakai split ";"
                    var dataText = i.split(";")

                    //kalo dataText[0] --> Tanggalnya sama dengan pilihan user
                    if(dataText[0].equals(format())){
                        //ambil dataText[1] pesannya terus masukin ke var pesan untuk ditampilkan
                        pesan = pesan + format() + " "+dataText[1] + "\n"

                        //artinya sudah ketemu memo yang tanggalnya sesuai
                        ketemu = true
                    }
                }
                TxtViewIsiPesan.text = pesan

                //sama sekali g ada memo yang tanggalnya sesuai dengan permintaan user
                if(ketemu==false)
                    TxtViewIsiPesan.text = "Tidak ada data pada tanggal ini"
            }
        }
    }
}
