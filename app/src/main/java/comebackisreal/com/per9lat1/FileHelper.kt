package comebackisreal.com.per9lat1

import android.content.Context
import android.os.Environment
import android.util.Log
import android.view.ContextThemeWrapper
import android.widget.Toast
import java.io.File

class FileHelper {
    fun writeFile(data : String, context : Context, oldData : String){
        if(isExternalStorageWritable()){
            val fileName:String = "myPrivateFile2.txt"
            var file = File(context.getExternalFilesDir(null), fileName)

            try{
                var tmp = ""
                if(oldData==""){
                    tmp = data
                }
                else {
                    tmp = oldData + "\n" + data
                }
                file.writeBytes(tmp.toByteArray())
            }catch (e:Exception){
                file.writeBytes(data.toByteArray())
                Toast.makeText(context, "File Not Found, making new File", Toast.LENGTH_SHORT).show()
            }
        }
        else{
            Toast.makeText(context, "Cant Write to External Storage", Toast.LENGTH_SHORT).show()
        }
    }

    fun readFile(context: Context) : String {
        if(isExternalStorageReadable()){
            try{
                val fileName:String = "myPrivateFile2.txt"
                var file = File(context.getExternalFilesDir(null), fileName)
                var contents = file.readText()
                return contents
            }catch (e:Exception){
                return ""
            }
        }
        else{
            Toast.makeText(context,"Can't read External Storage", Toast.LENGTH_SHORT).show()
            return ""
        }

    }

    /* Checks if external storage is available for read and write */
    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /* Checks if external storage is available to at least read */
    fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }
}